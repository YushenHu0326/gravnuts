//define the gravity elements.
//gravity angle is the actual gravity direction in game
//gravity scale is the parameter that control the overall gravity direction
globalvar gravityAngle;
globalvar gravityScale;

//the default value is 0
gravityAngle = 0;
gravityScale = 0;