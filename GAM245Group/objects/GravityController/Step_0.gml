//=============================================================================//
//this section is the input to change the gravity direction
//using arrow keys to control the direction and set the scale
if (keyboard_check_pressed(vk_left) && gravityScale == gravityAngle){
	if (gravityScale == 360){
		gravityScale = 90;
		gravityAngle = 0;
	}
	else{
		gravityScale += 90;
	}
}
else if (keyboard_check_pressed(vk_right) && gravityScale == gravityAngle){
	if (gravityScale == 0){
		gravityScale = 270;
		gravityAngle = 360;
	}
	else{
		gravityScale -= 90;
	}
}
//=============================================================================//

//=============================================================================//
//this section is to apply subtle change to the gravity direction
//if the gravity changes a lot in one frame, it will be unnatural
//so the variable "gravityAngle" is the actual gravity parameter in game
if (gravityAngle < gravityScale) {
	gravityAngle += 5;
}
else if (gravityAngle > gravityScale) {
	gravityAngle -= 5;
}

if (Player.x < 512) {
	with (all) {
		x += 2048;
	}
}
else if (Player.x > room_width - 512) {
	with (all) {
		x -= 2048;
	}
}
//=============================================================================//
