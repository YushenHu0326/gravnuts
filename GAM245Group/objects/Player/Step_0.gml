//===========================================================//
//the following code controls the player's parameters' change with gravity
//the change applys to position only, there is no change on player's image angle
if (gravityAngle < gravityScale){
	move_radius = point_distance(x, y, room_width / 2, room_height / 2);
	move_angle = point_direction(room_width / 2, room_height / 2, x, y) + 5 * (90 / 85);
	
	x = room_width / 2 + move_radius * dcos(move_angle);
	y = room_height / 2 - move_radius * dsin(move_angle);
}
else if (gravityAngle > gravityScale){
	move_radius = point_distance(x, y, room_width / 2, room_height / 2);
	move_angle = point_direction(room_width / 2, room_height / 2, x, y) - 5 * (90 / 85);
	
	x = room_width / 2 + move_radius * dcos(move_angle);
	y = room_height / 2 - move_radius * dsin(move_angle);
}
//===========================================================//

//===========================================================//
//the following code controls the input of walking
if (keyboard_check(ord("A")) && !place_meeting(x - 8, y, TestBlocks)) {
	x -= 3;
	sprite_index = POC_SpacemanSprite_Run_Left;
}
else if (keyboard_check(ord("D"))  && !place_meeting(x + 8, y, TestBlocks)) {
	x += 3;
	sprite_index = POC_SpacemanSprite_Run_Right;
}
else{
	sprite_index = POC_SpacemanSprite_Idle;
}
//===========================================================//

//===========================================================//
//the following code controls the player's falling condition
if (place_meeting(x, y + 8 + vspeed, TestBlocks)){
	vspeed = 0;
	if (fallToDeath) {
		room_goto(End);
	}
}
else{
	vspeed += 0.2;
}

if (keyboard_check_pressed(vk_space) && vspeed == 0){
	vspeed = -5;
}
//===========================================================//

//===========================================================//
//the following code controls player's shooting with machinegun
//when colliding with machinegun shooting position while not fallling
//player has the ability to control the direction of the turret
if (place_meeting(x, y, MachinegunShootingPos) && vspeed == 0 && gravityAngle == gravityScale) {
	if (mouse_check_button(mb_left)) {	
		with (MachinegunTurret) {
			image_angle = point_direction(mouse_x, mouse_y, x, y) + 90;
			if (image_angle < 120) {
				image_angle = 120;
			}
			else if (image_angle > 240) {
				image_angle = 240;
			}
			
			//shooting
			var bullet = instance_create_depth(x, y, -10000, MachinegunBullet);
			with (bullet) {
				speed = 10;
				direction = MachinegunTurret.image_angle + 90;
			}
		}
	}
}
//===========================================================//

//===========================================================//
//the following code handles player controlling with bombergun
if (place_meeting(x, y, BombergunShootingPos) && vspeed == 0 && gravityAngle == gravityScale) {
	if (mouse_check_button(mb_left) && bombCD <= 0) {	
		with (BombergunTurret) {
			image_angle = point_direction(mouse_x, mouse_y, x, y) + 90;
			if (image_angle < 120) {
				image_angle = 120;
			}
			else if (image_angle > 240) {
				image_angle = 240;
			}
			
			//shooting
			var bomb = instance_create_depth(x, y, -10000, Bomb);
			with (bomb) {
				randomize();
				speed = random_range(2, 6);
				
				randomize();
				direction = BombergunTurret.image_angle + 90 + random_range(-30, 30);
			}
			
			bombCD = 10;
		}
	}
}

if (bombCD > 0) {
	bombCD -= 1 / room_speed;
}

//the following code handles player shooting missile
if (place_meeting(x, y, MissileShootingPos) && vspeed == 0 && gravityAngle == gravityScale) {
	if (mouse_check_button(mb_left) && missileCD <= 0) {	
		with (MissileTurret) {
			image_angle = point_direction(mouse_x, mouse_y, x, y) + 90;
			if (image_angle < 120) {
				image_angle = 120;
			}
			else if (image_angle > 240) {
				image_angle = 240;
			}
			
			//shooting
			instance_create_depth(x, y, -10000, Missile);
			
			missileCD = 3;
		}
	}
}

if (missileCD > 0) {
	missileCD -= 1 / room_speed;
}