//find the closest asteroid
var target;
var distance = 99999999;
for (var i; i < instance_number(Asteroid); i++) {
	if (distance_to_object(instance_find(Asteroid, i)) < distance) {
		target = instance_find(Asteroid, i);
		distance = distance_to_object(instance_find(Asteroid, i));
	}
}

speed = 20;
direction = point_direction(x, y, target.x, target.y);

//self destroy after 5 seconds
alarm[0] = 10 / room_speed;