//===========================================================//
//the following code controls the object's parameters' change with gravity
if (gravityAngle < gravityScale){
	move_radius = point_distance(x, y, room_width / 2, room_height / 2);
	move_angle = point_direction(room_width / 2, room_height / 2, x, y) + 5 * (90 / 85);
	
	x = room_width / 2 + move_radius * dcos(move_angle);
	y = room_height / 2 - move_radius * dsin(move_angle);
	
	image_angle += 5 * (90 / 85);
}
else if (gravityAngle > gravityScale){
	move_radius = point_distance(x, y, room_width / 2, room_height / 2);
	move_angle = point_direction(room_width / 2, room_height / 2, x, y) - 5 * (90 / 85);
	
	x = room_width / 2 + move_radius * dcos(move_angle);
	y = room_height / 2 - move_radius * dsin(move_angle);
	
	image_angle -= 5 * (90 / 85);
}
//===========================================================//