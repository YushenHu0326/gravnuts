if (attackDirection == 3) {
	if (charlieIsShooting) {
		timeBetweenShot -= 1 / room_speed;
	}
	else{
		timeBetweenSpawn -= 1 / room_speed;
	}
}

//control the state of spawner
//if is shooting, while the attack wave is passed
//stop shooting, generate a new shoot wave length
//else if is not shooting, while the attack wave
//is coming, generate a new timespan between spawning
//and attack
if (charlieIsShooting && timeBetweenShot <= 0) {
	randomize();
	charlieIsShooting = !charlieIsShooting;
	
	timeBetweenShot = random_range(30, 60);
	//generate a new attack direction
	randomize();
	attackDirection = irandom_range(1, 4);
}
else if (!charlieIsShooting && timeBetweenSpawn <= 0) {
	randomize();
	charlieIsShooting = !charlieIsShooting;
	
	timeBetweenSpawn = random_range(5, 15);
}

//actually control the shooting
if (charlieIsShooting) {
	if (shootingGap <= 0) {
		randomize();
		xdev = random_range(-50, 50);
		ydev = random_range(-50, 50);
		instance_create_depth(x + xdev, y + ydev, -10000, Asteroid);
		randomize();
		shootingGap = random_range(0.5, 2);
	}
	shootingGap -= 1 / room_speed;
}

//===========================================================//
//the following code controls the object's parameters' change with gravity
if (gravityAngle < gravityScale){
	move_radius = point_distance(x, y, room_width / 2, room_height / 2);
	move_angle = point_direction(room_width / 2, room_height / 2, x, y) + 5 * (90 / 85);
	
	x = room_width / 2 + move_radius * dcos(move_angle);
	y = room_height / 2 - move_radius * dsin(move_angle);
	
	image_angle += 5 * (90 / 85);
}
else if (gravityAngle > gravityScale){
	move_radius = point_distance(x, y, room_width / 2, room_height / 2);
	move_angle = point_direction(room_width / 2, room_height / 2, x, y) - 5 * (90 / 85);
	
	x = room_width / 2 + move_radius * dcos(move_angle);
	y = room_height / 2 - move_radius * dsin(move_angle);
	
	image_angle -= 5 * (90 / 85);
}
//===========================================================//