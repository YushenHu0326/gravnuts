if (room == POC_Teamwork_room && Player.vspeed > 15) {
	fallToDeath = true;
}
else if (room == End) {
	if (keyboard_check_pressed(vk_enter)){
		fallToDeath = false;
		health = 100;
		room_goto(POC_Teamwork_room);
	}
}

if (keyboard_check_pressed(ord("E"))){
	room_goto(End);
}
else if (keyboard_check_pressed(ord("R"))){
	fallToDeath = false;
	health = 100;
	room_goto(POC_Teamwork_room);
}
else if (keyboard_check_pressed(ord("T"))){
	debuggerOn = !debuggerOn;
}