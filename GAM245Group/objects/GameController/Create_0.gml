globalvar fallToDeath;
globalvar spaceshipHealth;
globalvar levelOfDifficulty;
globalvar attackDirection;

health = 100;

debuggerOn = true;
fallToDeath = false;
spaceshipHealth = 100;
//By default the level of difficulty is set to 2
levelOfDifficulty = 2;

attackDirection = 1;