switch (room) {
	case POC_Teamwork_room:
		draw_text(20, 20, string(spaceshipHealth));
		if (AsteroidSpawner_alpha.alphaIsShooting) {
			draw_text(20, 40, "Asteroids come from Alpha");
		}
		else if (AsteroidSpawner_bravo.bravoIsShooting) {
			draw_text(20, 40, "Asteroids come from Bravo");
		}
		else if (AsteroidSpawner_charlie.charlieIsShooting) {
			draw_text(20, 40, "Asteroids come from Charlie");
		}
		else if (AsteroidSpawner_delta.deltaIsShooting) {
			draw_text(20, 40, "Asteroids come from Delta");
		}
		draw_text(20, 60, string(attackDirection));
		break;
	case End:
		draw_text(10, 10, "You fall to death");
		draw_text(10, 25, "Press Enter to restart");
		break;
}