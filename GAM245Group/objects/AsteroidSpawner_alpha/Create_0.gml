randomize();
//time between spawn refers to the time between the last attack
//wave and the next attack wave

//time between shoot refers to the length of attack wave
timeBetweenSpawn = random_range(5, 15);
timeBetweenShot = random_range(30, 60);

//is shooting refers to the state of whether the spawner is shooting
alphaIsShooting = false;

//the first wave always come from alpha, then random direction
//the gap time between spawning, a random value between 1 and 3
randomize();
shootingGap = random_range(1, 3)